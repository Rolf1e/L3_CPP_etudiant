
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>

// liste d'entiers avec itérateur
class Liste {
private:
    struct Noeud {
        int _valeur;
        Noeud *_ptrNoeudSuivant;
    };

    Noeud *_ptrTete;

public:
    class iterator {
    private:
        Noeud *_ptrNoeudCourant;

    public:

        iterator(Noeud *ptrNoeudCourant) {
            _ptrNoeudCourant = ptrNoeudCourant;
        }

        const iterator &operator++() {
            _ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
            return *this;
        }

        int &operator*() const {
            return _ptrNoeudCourant->_valeur;
        }

        bool operator!=(const iterator &iter) const {
            return _ptrNoeudCourant != iter._ptrNoeudCourant;
        }

        friend Liste;
    };

    Liste() {
        _ptrTete = nullptr;
    }

    ~Liste() {
        clear();
    }

    void push_front(int value) {
        if (_ptrTete == nullptr) {
            _ptrTete = new Noeud{value, nullptr};
            return;
        }
        _ptrTete = new Noeud{value, _ptrTete};
    }

    int &front() const {
        return _ptrTete->_valeur;
    }

    void clear() {
        while (_ptrTete != nullptr) {
            Noeud *actual = _ptrTete;
            _ptrTete = _ptrTete->_ptrNoeudSuivant;
            delete actual;
        }
    }

    bool empty() const {
        return _ptrTete == nullptr;
    }

    iterator begin() const {
        return _ptrTete;
    }

    iterator end() const {
        return nullptr;
    }

};

std::ostream &operator<<(std::ostream &os, const Liste &liste) {
    auto iterator = liste.begin();
    while (iterator != liste.end()) {
        os << *iterator << " ";
        ++iterator;
    }
    return os;
}

#endif

