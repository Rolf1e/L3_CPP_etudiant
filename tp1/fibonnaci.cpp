using namespace std;

#include <iostream>
#include "fibonnaci.hpp"

int fibonacciRecursif(int x) {
	if((x==1)||(x==0)) {
      return(x);
    } else {
      return(fibonacciRecursif(x-1)+fibonacciRecursif(x-2));
    }
}

int fibonacciIteratif(int num) {
   int x = 0, y = 1, z = 0;
   for (int i = 0; i < num; i++) {
      z = x + y;
      x = y;
      y = z;
   }
   return x;
}