#ifndef __VEC_HPP
#define __VEC_HPP

class Vector3D
{
	private:
		float _X;
		float _Y;
		float _Z;

	public:
		Vector3D(float X, float Y, float Z);
		void display();
		float norme();
};

struct vector3d
{
	float X;
	float Y;
	float Z;
};

void display(vector3d vector);
float norme(vector3d vector);
#endif