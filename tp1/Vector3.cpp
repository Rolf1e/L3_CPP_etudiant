#include <iostream>
#include "Vector3.hpp"
#include <cmath>
using namespace std;

Vector3D::Vector3D(float X, float Y, float Z):
	_X(X), _Y(Y), _Z(Z)
{}

void Vector3D::display() {
	std::cout << "Vector :"
		<< "(" << _X
		<< ", " << _Y
		<< ", " << _Z << ")"
		<< std::endl;
}

float Vector3D::norme() {
	return std::sqrt(pow(_X, 2) + pow(_Y, 2) + pow(_Z, 2));
}

void display(vector3d vector) {
	std::cout << "Vector :" 
		<< "(" << vector.X
		<< ", " << vector.Y
		<< ", " << vector.Z << ")" << std::endl;
}

float norme(vector3d vector) {
	return std::sqrt(pow(vector.X, 2) + pow(vector.Y, 2) + pow(vector.Z, 2));
}


