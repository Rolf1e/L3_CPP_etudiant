using namespace std;
#include <iostream>
#include "fibonnaci.hpp"
#include "Vector3.hpp"

int main(int argc, char **argv)
{
	cout << "Hello word" << endl;
	cout << fibonacciRecursif(7) << endl;
	cout << fibonacciIteratif(7) << endl;

	cout << "STRUCT" << endl;
	vector3d vectorStruc = {2, 3, 6};
	display(vectorStruc);
	cout << "norme: " << norme(vectorStruc) << endl;

	cout << "OBJECT" << endl;
	Vector3D vector(2, 3, 6);
	vector.display();
	cout << "norme : " << vector.norme() << endl;


	return 0;
}