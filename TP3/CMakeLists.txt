cmake_minimum_required( VERSION 3.0 )
project( TP_vide )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )

# programme principal
add_executable( main_tp3.out
        src/main/main.cpp
        src/main/Location.cpp
        src/main/Client.cpp
        src/main/Produit.cpp
        src/main/Magasin.hpp
        src/main/Magasin.cpp)
target_link_libraries( main_tp3.out)

# programme de test
add_executable( main_test_tp3.out
        src/test/main_test.cpp
        src/test/Client_test.cpp
        src/main/Client.cpp
        src/test/Produit_test.cpp
        src/main/Produit.cpp
        src/test/Magasin_test.cpp
        src/main/Magasin.cpp
        src/main/Location.cpp ../TP4/src/test/main_test.cpp)
target_link_libraries( main_test_tp3.out ${PKG_CPPUTEST_LIBRARIES} )
