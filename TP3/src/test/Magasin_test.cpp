//
// Created by rolfie on 3/23/20.
//

#include <CppUTest/CommandLineTestRunner.h>
#include "../main/Produit.hpp"
#include "../main/Magasin.hpp"


TEST_GROUP(GroupMagasin) {
};


TEST(GroupMagasin, addClient) {
    Magasin magasin = Magasin();
    magasin.ajouterClient("Fabien");
    CHECK_EQUAL(magasin.nbClients(), 1);
}

TEST(GroupMagasin, delClient) {
    Magasin magasin = Magasin();
    magasin.ajouterClient("Fabien");
    magasin.supprimerClient(0);
    CHECK_EQUAL(magasin.nbClients(), 0);
}

TEST(GroupMagasin, errDelClient) {
    Magasin magasin = Magasin();
    magasin.ajouterClient("Fabien");
    CHECK_THROWS(string, magasin.supprimerClient(1));
}

TEST(GroupMagasin, addProduit) {
    Magasin magasin = Magasin();
    magasin.ajouterProduits("Pelle");
    CHECK_EQUAL(magasin.nbProduits(), 1);
}

TEST(GroupMagasin, delProduit) {
    Magasin magasin = Magasin();
    magasin.ajouterProduits("Pelle");
    magasin.supprimerProduits(0);
    CHECK_EQUAL(magasin.nbProduits(), 0);
}

TEST(GroupMagasin, errDelProduit) {
    Magasin magasin = Magasin();
    magasin.ajouterProduits("Pelle");
    CHECK_THROWS(string, magasin.supprimerProduits(1));
}

TEST(GroupMagasin, nbLocat) {
    Magasin magasin = Magasin();
    magasin.ajouterLocations(1, 1);
    CHECK_EQUAL(1, magasin.nbLocations());
}

TEST(GroupMagasin, delLoc) {
    Magasin magasin = Magasin();
    magasin.ajouterLocations(1, 1);
    magasin.supprimerLocation(1, 1);
    CHECK_EQUAL(0, magasin.nbLocations());
}

TEST(GroupMagasin, errDelLoc) {
    Magasin magasin = Magasin();
    magasin.ajouterLocations(1, 1);
    CHECK_THROWS(string, magasin.supprimerLocation(2, 2));
}

TEST(GroupMagasin, isIn) {
    Magasin magasin = Magasin();
    magasin.ajouterLocations(1, 1);
    CHECK_TRUE(magasin.trouverClientDansLocation(1))
    CHECK_TRUE(magasin.trouverProduitDansLocation(1))
}

TEST(GroupMagasin, clientInLoc) {
    Magasin magasin = Magasin();
    magasin.ajouterClient("Fabien");
    magasin.ajouterClient("Quentin");
    magasin.ajouterProduits("Pelle");
    magasin.ajouterLocations(1, 1);
    magasin.trouverClientDansLocation(1);

    vector<int> produit =  {1};
    vector<int>  client =  {1};

    CHECK_EQUAL(produit[0],magasin.calculerProduitsLibres()[0]);
    CHECK_EQUAL(client[0],magasin.calculerClientsLibres()[0]);
}




