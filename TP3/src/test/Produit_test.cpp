//
// Created by rolfie on 3/23/20.
//

#include <CppUTest/CommandLineTestRunner.h>
#include "../main/Produit.hpp"


TEST_GROUP(GroupProduit) {
};

TEST(GroupProduit, getters) {
    Produit produit = Produit(0, "une pelle");
    CHECK_EQUAL(produit.getId(), 0);
    CHECK_EQUAL(produit.getDescription(), "une pelle");
}