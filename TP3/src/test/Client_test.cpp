//
// Created by rolfie on 3/23/20.
//
#include <CppUTest/CommandLineTestRunner.h>
#include "../main/Client.hpp"


TEST_GROUP(GroupClient) {
};

TEST(GroupClient, getters) {
    Client client = Client(0, "Fabien");
    CHECK_EQUAL(client.getId(), 0);
    CHECK_EQUAL(client.getNom(), "Fabien");
}