//
// Created by rolfie on 3/23/20.
//
#include <iostream>
#import "Location.hpp"
#include "Client.hpp"
#include "Magasin.hpp"

int main(int argc, char **argv) {
    Magasin magasin = Magasin();

    magasin.ajouterClient("Fabien");
    magasin.ajouterClient("Nicolas");
    magasin.afficherClients();

    magasin.supprimerClient(0);
    magasin.afficherClients();
}
