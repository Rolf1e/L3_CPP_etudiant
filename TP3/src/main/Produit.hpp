//
// Created by rolfie on 3/23/20.
//
#include <iostream>

using namespace std;

#ifndef TP_VIDE_PRODUIT_HPP
#define TP_VIDE_PRODUIT_HPP

class Produit {
private:
    int _id;
    string _description;
public:
    Produit(int id, const string &description);

    int getId() const;

    const string &getDescription() const;

    void afficherProduit() const;
};

#endif //TP_VIDE_PRODUIT_HPP
