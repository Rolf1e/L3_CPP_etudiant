//
// Created by rolfie on 3/23/20.
//
#include <iostream>
#include <exception>
#include "Magasin.hpp"

Magasin::Magasin() :
        _idCourantClient(0), _idCourantProduit(0) {}

//Clients
int Magasin::nbClients() const {
    return _clients.size();
}

void Magasin::ajouterClient(const string &nom) {
    _clients.push_back(Client(_idCourantClient, nom));
    _idCourantClient++;
}

void Magasin::afficherClients() const {
    for (const auto &_client : _clients) {
        _client.afficherClient();
    }
}

void Magasin::supprimerClient(int idClient) {
    vector<Client> temp;
    for (const Client &_client : _clients) {
        if (_client.getId() != idClient) {
            temp.push_back(_client);
        }
    }
    if (temp.size() == _clients.size()) {
        throw string("ERREUR: client doesn't exist");
    }
    _clients.swap(temp);
}

//Produits
int Magasin::nbProduits() const {
    return _produits.size();
}

void Magasin::ajouterProduits(const string &nom) {
    _produits.push_back(Produit(_idCourantClient, nom));
    _idCourantProduit++;
}

void Magasin::afficherProduits() const {
    for (const auto &_produit : _produits) {
        _produit.afficherProduit();
    }
}

void Magasin::supprimerProduits(int idProduit) {
    vector<Produit> temp;
    for (const Produit &_produit : _produits) {
        if (_produit.getId() != idProduit) {
            temp.push_back(_produit);
        }
    }
    if (temp.size() == _produits.size()) {
        throw string("ERREUR: product doesn't exist");
    }
    _produits.swap(temp);
}

//Locations

int Magasin::nbLocations() const {
    return _locations.size();
}

void Magasin::ajouterLocations(int idClient, int idProduit) {
    if (_locations.size() == 0) {
        _locations.push_back(Location(idClient, idProduit));
        return;
    }

    for (const Location &_location: _locations) {
        if (_location.getIdClient() == idClient &&
            _location.getIdProduit() == idProduit) {
            throw string("ERREUR: location exist");
        }
    }

    _locations.push_back(Location(idClient, idProduit));
}

void Magasin::afficherLocations() const {
    for (const Location &_location: _locations) {
        _location.afficherLocation();
    }
}

void Magasin::supprimerLocation(int idClient, int idProduit) {
    vector<Location> temp;
    for (const Location &_location: _locations) {
        if (_location.getIdClient() != idClient &&
            _location.getIdProduit() != idProduit) {
            temp.push_back(_location);
        }
    }
    if (temp.size() == _locations.size()) {
        throw string("ERREUR: product doesn't exist");
    }
    _locations.swap(temp);
}

bool Magasin::trouverClientDansLocation(int idClient) {
    for (const Location &_location: _locations) {
        if (_location.getIdClient() == idClient) {
            return true;
        }
    }
    return false;
}

vector<int> Magasin::calculerClientsLibres() {
    vector<int> clientLibres;
    for (const Location &_location: _locations) {
        for (const Client &_client: _clients) {
            if (trouverProduitDansLocation(_client.getId())) {
                clientLibres.push_back(_location.getIdClient());
            }
        }
    }
    return clientLibres;
}

bool Magasin::trouverProduitDansLocation(int idProduit) {
    for (const Location &_location: _locations) {
        if (_location.getIdClient() == idProduit) {
            return true;
        }
    }
    return false;
}

vector<int> Magasin::calculerProduitsLibres() {
    vector<int> clientLibres;
    for (const Location &_location: _locations) {
        for (const Produit &_produit: _produits) {
            if (trouverProduitDansLocation(_produit.getId())) {
                clientLibres.push_back(_location.getIdProduit());
            }
        }
    }
    return clientLibres;
}






