#include <iostream>

using namespace std;

#include "Location.hpp"

//
// Created by rolfie on 3/23/20.
//
Location::Location(int idClient, int idProduit) :
        _idClient(idClient), _idProduit(idProduit) {}

void Location::afficherLocation() const {
    cout << "New location: (" << _idClient << ", " << _idProduit << ")" << endl;
}

int Location::getIdClient() const {
    return _idClient;
}

int Location::getIdProduit() const {
    return _idProduit;
}
