//
// Created by rolfie on 3/23/20.
//

#ifndef TP_VIDE_LOCATION_HPP
#define TP_VIDE_LOCATION_HPP

class Location {
private:
    int _idClient;
    int _idProduit;
public:
    Location(int idClient, int idProduit);
    void afficherLocation() const;
    int getIdClient() const;
    int getIdProduit() const;

};

#endif //TP_VIDE_LOCATION_HPP
