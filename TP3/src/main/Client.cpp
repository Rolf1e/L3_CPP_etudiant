//
// Created by rolfie on 3/23/20.
//
#include <iostream>
#include "Client.hpp"

using namespace std;

Client::Client(int id, const string &nom) : _id(id), _nom(nom) {}

int Client::getId() const {
    return _id;
}

const string &Client::getNom() const {
    return _nom;
}

void Client::afficherClient() const {
    cout << "New Client: (" << _id << ", " << _nom << ")" << endl;
}