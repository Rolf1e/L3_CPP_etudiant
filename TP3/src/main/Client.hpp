//
// Created by rolfie on 3/23/20.
//
#include <iostream>

using namespace std;

#ifndef TP_VIDE_CLIENT_HPP
#define TP_VIDE_CLIENT_HPP

class Client {
private:
    int _id;
    string _nom;
public:
    Client(int id, const string &nom);

    int getId() const;

    const string &getNom() const;

    void afficherClient() const;
};

#endif //TP_VIDE_CLIENT_HPP
