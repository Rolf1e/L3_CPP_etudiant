//
// Created by rolfie on 3/23/20.
//
#include <iostream>
#include <vector>
#include "Client.hpp"
#include "Produit.hpp"
#include "Location.hpp"

using namespace std;

#ifndef TP_VIDE_MAGASIN_HPP
#define TP_VIDE_MAGASIN_HPP

class Magasin {
private:
    vector<Client> _clients;
    vector<Produit> _produits;
    vector<Location> _locations;
    int _idCourantClient;
    int _idCourantProduit;
public:
    Magasin();

//    Clients
    int nbClients() const;

    void ajouterClient(const string &nom);

    void afficherClients() const;

    void supprimerClient(int idClient);

//    Produits

    int nbProduits() const;

    void ajouterProduits(const string &nom);

    void afficherProduits() const;

    void supprimerProduits(int idProduit);

    //Locations

    int nbLocations() const;

    void ajouterLocations(int idClient, int idProduit);

    void afficherLocations() const;

    void supprimerLocation(int idClient, int idProduit);

    bool trouverClientDansLocation(int idClient);

    vector<int> calculerClientsLibres();

    bool trouverProduitDansLocation(int idProduit);

    vector<int> calculerProduitsLibres();
};

#endif //TP_VIDE_MAGASIN_HPP
