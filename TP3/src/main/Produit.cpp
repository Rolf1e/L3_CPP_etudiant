//
// Created by rolfie on 3/23/20.
//
#include <iostream>

#include "Produit.hpp"

Produit::Produit(int id, const string &description) :
        _id(id), _description(description) {}

int Produit::getId() const {
    return _id;
}

const string &Produit::getDescription() const {
    return _description;
}

void Produit::afficherProduit() const {
    cout << "New product: (" << _id << ", " << _description << ")" << endl;
}