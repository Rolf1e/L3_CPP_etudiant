#include "Liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) {
};

TEST(GroupListe, ajouterDevant) {
    Liste liste;
    liste.ajouterDevant(1);
    CHECK_EQUAL(1, liste._tete->_valeur);
}

TEST(GroupListe, getTaille) {
    Liste liste;
    CHECK_EQUAL(0, liste.getTaille());
}

TEST(GroupListe, getElement) {
    Liste liste;
    liste.ajouterDevant(1);
    liste.ajouterDevant(2);
    liste.ajouterDevant(3);
    liste.ajouterDevant(4);
    liste.ajouterDevant(5);
    CHECK_EQUAL(2, liste.getElement(3));
}