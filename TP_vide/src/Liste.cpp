#include "Liste.hpp"

Liste::Liste() {
    _tete = nullptr;
}

Liste::~Liste() {
    while (_tete != nullptr) {
        Noeud *actual = _tete;
        _tete = actual->_suivant;
        delete actual;
    }
}

void Liste::ajouterDevant(int valeur) {
    Noeud *parcours = _tete;

    if (_tete == nullptr) {
        _tete = new Noeud{valeur, _tete};
    } else {
        while (parcours != nullptr) {
            if (parcours->_suivant == nullptr) {
                _tete = new Noeud{valeur, _tete};
                return;
            }
            parcours = parcours->_suivant;
        }
    }
}

int Liste::getTaille() const {
    int taille = 0;
    Noeud *parcours = _tete;
    while (parcours != nullptr) {
        taille++;
        parcours = parcours->_suivant;
    }
    return taille;
}

int Liste::getElement(int indice) const {
    int index = 0;
    Noeud *parcours = _tete;
    while (parcours != nullptr) {
        if (index == indice) {
            return parcours->_valeur;
        }
        parcours = parcours->_suivant;
        index++;
    }
}