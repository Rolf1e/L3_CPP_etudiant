#include "Doubler.hpp"

#include <iostream>
#include <vector>

void pointers() {
    int a = 42;
    std::cout << a << std::endl;
    int *pa = &a;
    std::cout << *pa << std::endl;
    *pa = 37;
    std::cout << a << std::endl;
}

void dynamicPointers() {
   int *t1 = new int[10];
   t1[2] = 42;
   for(int i = 0; i < 10;  i++) {
       std::cout << t1[i] << " ";
   }
   delete [] t1;
   t1 = nullptr;
   if( t1 == nullptr) {
       std::cout << "\nnull";
   }
}

int main() {
    std::cout << doubler(21) << std::endl;
    std::cout << doubler(21.f) << std::endl;
    pointers();
    dynamicPointers();
    return 0;
}

