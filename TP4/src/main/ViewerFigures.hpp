//
// Created by rolfie on 4/9/20.
//

#ifndef L3_CPP_TP5_VIEWERFIGURES_HPP
#define L3_CPP_TP5_VIEWERFIGURES_HPP

#include <gtkmm.h>
#include "ZoneDessin.hpp"

class ViewerFigures {
private:
    Gtk::Main _kit;
    Gtk::Window _window;
    ZoneDessin zoneDessin;

public:
    ViewerFigures(int argc, char **argv);

    void run();

};

#endif //L3_CPP_TP5_VIEWERFIGURES_HPP
