//
// Created by rolfie on 3/23/20.
//

#ifndef TP_VIDE_FIGUREGEOMETRIQUE_HPP
#define TP_VIDE_FIGUREGEOMETRIQUE_HPP

#include <cairomm/refptr.h>
#include <cairomm/context.h>
#include "Couleur.hpp"

class FigureGeometrique {

protected:
    Couleur _couleur;
public:
    FigureGeometrique(const Couleur &couleur);

    Couleur getCouleur() const;

    virtual void afficher(const Cairo::RefPtr<Cairo::Context> context) const;

    virtual ~FigureGeometrique() {};
};

#endif //TP_VIDE_FIGUREGEOMETRIQUE_HPP
