//
// Created by rolfie on 3/23/20.
//


#include "FigureGeometrique.hpp"
#include "Couleur.hpp"

using namespace std;

FigureGeometrique::FigureGeometrique(const Couleur &couleur) :
        _couleur(couleur) {}

Couleur FigureGeometrique::getCouleur() const {
    return _couleur;
}

void FigureGeometrique::afficher(const Cairo::RefPtr<Cairo::Context> context) const {
}
