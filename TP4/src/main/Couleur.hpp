//
// Created by rolfie on 3/23/20.
//

#ifndef TP_VIDE_COULEUR_HPP
#define TP_VIDE_COULEUR_HPP

#include <iostream>

using namespace std;

struct Couleur {
    double _r;
    double _g;
    double _b;

    Couleur(double r, double g, double b) : _r(r), _g(g), _b(b) {}

    string afficher() const {
        return to_string(_r) + "_" + to_string(_g) + "_" + to_string(_b);
    }
};

#endif //TP_VIDE_COULEUR_HPP
