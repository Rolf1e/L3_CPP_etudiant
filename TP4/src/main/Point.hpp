//
// Created by rolfie on 3/23/20.
//

#ifndef TP_VIDE_POINT_HPP
#define TP_VIDE_POINT_HPP
#include <iostream>

using namespace std;

struct Point {
    int _x;
    int _y;

    Point(): _x(0), _y(0){}
    Point(int x, int y) : _x(x), _y(y) {}
    string afficher() const{
        return to_string(_x) + "_" + to_string(_y);
    }
};

#endif //TP_VIDE_POINT_HPP
