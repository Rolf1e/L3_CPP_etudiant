//
// Created by rolfie on 3/23/20.
//

#ifndef TP_VIDE_LIGNE_HPP
#define TP_VIDE_LIGNE_HPP

#include "Point.hpp"
#include "Couleur.hpp"
#include "FigureGeometrique.hpp"

class Ligne : public FigureGeometrique {
private:
    Point _p0;
    Point _p1;

public:
    Ligne(const Couleur &couleur, const Point &p0, const Point &p1);

    virtual void afficher(const Cairo::RefPtr<Cairo::Context> context) const override;

    const Point &getP0() const;

    const Point &getP1() const;
};

#endif //TP_VIDE_LIGNE_HPP
