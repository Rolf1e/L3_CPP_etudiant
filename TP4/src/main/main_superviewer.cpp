//
// Created by rolfie on 4/9/20.
//

#include <gtkmm.h>
#include "ViewerFigures.hpp"

int main(int argc, char *argv[]) {
    ViewerFigures viewerFigures(argc, argv);
    viewerFigures.run();
    return 0;
}