//
// Created by rolfie on 4/9/20.
//

#include "ZoneDessin.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"

using namespace std;

ZoneDessin::ZoneDessin() {
    _figures = {
            new Ligne(Couleur(1, 0, 0), Point(0, 0), Point(100, 200)),
            new PolygoneRegulier(Couleur(0, 1, 0), Point(300, 200), 50, 7)
    };
    add_events(Gdk::BUTTON_PRESS_MASK);
    signal_button_press_event().connect(sigc::mem_fun(*this, &ZoneDessin::gererClic));
};

ZoneDessin::~ZoneDessin() {
    for (FigureGeometrique *fG : _figures) {
        delete fG;
    }
}

bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> &context) {
    for (FigureGeometrique *fG : _figures) {
        fG->afficher(context);
    }
}

bool ZoneDessin::gererClic(GdkEventButton *event) {
    if (event->type == GDK_BUTTON_PRESS) {
        if (event->button == 1) {
            _figures.push_back(
                    new PolygoneRegulier(Couleur(rand() % 101 / 100.f, rand() % 101 / 100.f, rand() % 101 / 100.f),
                                         Point(event->x, event->y),
                                         rand() % 100, rand() % 10));
        }
        if (event->button == 3) {
            FigureGeometrique *fg = _figures.back();
            _figures.pop_back();
            delete fg;
        }
        get_window()->invalidate(false);
    }
    return true;
}