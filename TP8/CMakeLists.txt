cmake_minimum_required( VERSION 3.0 )
project( TP8 )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )

pkg_check_modules( PKG_GTK REQUIRED gtkmm-3.0 )
include_directories( ${PKG_GTK_INCLUDE_DIRS} )
add_executable( main_bouteille.out src/main/main_bouteille.cpp
        src/main/model/Bouteille.cpp
        src/main/controller/Controller.cpp
        src/main/model/Inventaire.cpp
        src/main/vue/Vue.cpp)
target_link_libraries( main_bouteille.out ${PKG_GTK_LIBRARIES} )

pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )
add_executable( main_test.out src/test/main_test.cpp
        src/main/model/Bouteille.cpp
        src/test/model/Bouteille_test.cpp
        src/main/controller/Controller.cpp
        src/main/model/Inventaire.cpp
        src/test/model/Inventaire_test.cpp
        src/main/vue/Vue.cpp)
target_link_libraries( main_test.out ${PKG_GTK_LIBRARIES} ${PKG_CPPUTEST_LIBRARIES} )
