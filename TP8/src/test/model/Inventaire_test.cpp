#include "../../main/model/Inventaire.hpp"

#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupInventaire) {
};

TEST(GroupInventaire, TestInventaire_1) {
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    Inventaire inventaire;
    inventaire.addBouteille(Bouteille{"cyanure", "2013-08-18", 0.25});
    inventaire.addBouteille(Bouteille{"mescaline", "2013-06-18", 0.1});
    std::ostringstream oss;
    // TODO decommenter
    oss << inventaire;
    CHECK_EQUAL("cyanure;2013-08-18;0,25\nmescaline;2013-06-18;0,1\n", oss.str());
    std::locale::global(vieuxLoc);
}

TEST(GroupInventaire, TestInventaire_2) {
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    Inventaire inventaire;
    std::istringstream iss("cyanure;2013-08-18;0,25\nmescaline;2013-06-18;0,1\n");
    // TODO decommenter
    iss >> inventaire;
    std::ostringstream oss;
    // TODO decommenter
    oss << inventaire;
    CHECK_EQUAL("cyanure;2013-08-18;0,25\nmescaline;2013-06-18;0,1\n", oss.str());
    std::locale::global(vieuxLoc);
}

TEST(GroupInventaire, TestInventaire_3) {
    auto funcCompare2Bouteilles =
            [](const Bouteille &b1, const Bouteille &b2) { return b1.getNom() < b2.getNom(); };
    Bouteille b1{"nom1", "date", 42};
    Bouteille b2{"nom0", "date", 37};
    CHECK_EQUAL(false, funcCompare2Bouteilles(b1, b2));
}

TEST(GroupInventaire, TestInventaire_4) {
    auto funcCompare2Bouteilles =
            [](const Bouteille &b1, const Bouteille &b2) { return b1.getNom() < b2.getNom(); };
    Bouteille b1{"nom1", "date", 42};
    Bouteille b2{"nom2", "date", 37};
    CHECK_EQUAL(true, funcCompare2Bouteilles(b1, b2));
}

