#include "controller/Controller.hpp"

int main(int argc, char ** argv) {
    Controller viewer(argc, argv);
    viewer.run();
	return 0;
}

