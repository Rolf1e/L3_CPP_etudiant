#include "Controller.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

Controller::Controller(int argc, char **argv) {
    _vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
    chargerInventaire("dumb");
    refresh();
}

void Controller::refresh() {
    for (auto &v : _vues) {
        _inventaire.sortInventory();
        v->actualiser();
    }
}

void Controller::run() {
    for (auto &v : _vues) {
        _inventaire.sortInventory();
        v->run();
    }
}

string Controller::getTexte() {
    ostringstream s;
    s << _inventaire;
    _inventaire.sortInventory();
    return s.str();
}

void Controller::chargerInventaire(const string &fileName) {
    ifstream myFile(fileName);
    if (myFile.is_open()) {
        myFile >> _inventaire;
    }
    refresh();
}


