#ifndef CONTROLEUR_HPP_
#define CONTROLEUR_HPP_

#include "../model/Inventaire.hpp"
#include "../vue/Vue.hpp"

#include <memory>
#include <vector>

// Controller : fait le lien entre la Vue et le Modèle. Point d'entrée de
// l'application. Utilisation : construire un Controller puis run().
class Controller {
private:
    Inventaire _inventaire;
    std::vector<std::unique_ptr<Vue>> _vues;

    void refresh();

public:
    Controller(int argc, char **argv);

    // Lance l'application.
    void run();

    string getTexte();

    void chargerInventaire(const string &fileName);
};

#endif
