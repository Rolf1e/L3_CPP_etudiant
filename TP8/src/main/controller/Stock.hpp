//
// Created by rolfie on 4/30/20.
//

#ifndef TP8_STOCK_HPP
#define TP8_STOCK_HPP

using namespace std;

class Stock {

private:
    map<string, float> _produits;

public:
    void recalculerStock(const Inventaire &inventaire);
};


#endif //TP8_STOCK_HPP
