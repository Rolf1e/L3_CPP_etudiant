#ifndef BOUTEILLE_HPP_
#define BOUTEILLE_HPP_

#include <iostream>
#include <string>

using namespace std;

// Modèle : une Bouteille est représentée par le nom du produit, la date de
// mise en bouteille et le volume de la bouteille.
class Bouteille {
private:
    std::string _nom;
    std::string _date;
    float _volume;

public:
    Bouteille();

    Bouteille(const string &nom, const string &date, const float &volume);

    string getNom() const;

    string getDate() const;

    float getVolume() const;

    void setNom(const string &nom);

    void setDate(const string &date);

    void setVolume(float volume);

    bool operator<(const Bouteille &b) const {
        if (_date < b.getDate()) {
            return true;
        } else if (_nom < b.getNom()) {
            return true;
        }
        return false;
    };
};

/// Flux de sortie au format "<nom>;<date>;<volume>\n".
std::istream &operator>>(std::istream &is, Bouteille &b);

/// Flux d'entrée au format "<nom>;<date>;<volume>\n".
std::ostream &operator<<(std::ostream &os, const Bouteille &b);

#endif
