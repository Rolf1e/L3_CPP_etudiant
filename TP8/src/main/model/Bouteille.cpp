#include "Bouteille.hpp"

#include <locale>

Bouteille::Bouteille(const string &nom, const string &date, const float &volume) :
        _nom(nom), _date(date), _volume(volume) {}

Bouteille::Bouteille() {}

std::istream &operator>>(std::istream &is, Bouteille &b) {
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    std::string buffer;
    if (std::getline(is, buffer, ';')) {
        b.setNom(buffer);
    }
    if (std::getline(is, buffer, ';')) {
        b.setDate(buffer);
    }
    if (std::getline(is, buffer, '\n')) {
        b.setVolume(std::stof(buffer));
    }
    std::locale::global(vieuxLoc);
    return is;
}

std::ostream &operator<<(std::ostream &os, const Bouteille &b) {
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    os << b.getNom() << ';' << b.getDate() << ';' << b.getVolume() << '\n';
    std::locale::global(vieuxLoc);
    return os;
}

std::string Bouteille::getNom() const {
    return _nom;
}

std::string Bouteille::getDate() const {
    return _date;
}

float Bouteille::getVolume() const {
    return _volume;
}

void Bouteille::setNom(const std::string &nom) {
    _nom = nom;
}

void Bouteille::setDate(const std::string &date) {
    _date = date;
}

void Bouteille::setVolume(float volume) {
    _volume = volume;
}
