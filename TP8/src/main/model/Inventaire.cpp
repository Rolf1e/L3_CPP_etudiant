#include <sstream>
#include "Inventaire.hpp"
#include <algorithm>

using namespace std;

istream &operator>>(std::istream &is, Inventaire &inventaire) {
    std::string buffer;
    Bouteille b;
    while (std::getline(is, buffer, '\n')) {
        std::istringstream istr(buffer);
        istr >> b;
        inventaire.addBouteille(b);
    }
    return is;
}

ostream &operator<<(ostream &os, const Inventaire &inventaire) {
    list<Bouteille> list = inventaire.getBouteilles();
    for_each(list.cbegin(), list.cend(),
             [&os](const Bouteille &b) {
                 os << b;
             });
    return os;
}

const list<Bouteille>& Inventaire::getBouteilles() const {
    return _bouteilles;
}

void Inventaire::addBouteille(const Bouteille &b) {
    _bouteilles.push_back(b);
}

void Inventaire::sortInventory() {
    _bouteilles.sort();
}
