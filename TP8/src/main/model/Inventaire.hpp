#ifndef INVENTAIRE_HPP_
#define INVENTAIRE_HPP_

#include "Bouteille.hpp"

#include <iostream>
#include <ostream>
#include <vector>
#include <list>

using namespace std;

// Modèle : inventaire de bouteilles.
class Inventaire {
private:
    std::list<Bouteille> _bouteilles;

public:
    const list <Bouteille> &getBouteilles() const;

    void addBouteille(const Bouteille &b);

    void sortInventory();

    friend istream &operator>>(std::istream &is, Inventaire &inventaire);

    friend ostream &operator<<(ostream &os, const Inventaire &inventaire);
};

#endif
