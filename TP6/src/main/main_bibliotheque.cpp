
#include <iostream>
#include "Livre.hpp"
#include "Bibliotheque.hpp"

int main() {
    Livre l("livre1", "Tigran", 1);
    Livre l2("livre2", "Quentin", 1);
    cout << l.getTitre() << endl;
    cout << l.getAuteur() << endl;

    Bibliotheque b;
    b.push_back(l);
    b.push_back(l2);

    b.afficher();

    return 0;
}

