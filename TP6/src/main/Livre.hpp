//
// Created by rolfie on 3/27/20.
//

#ifndef TP6_LIVRE_HPP
#define TP6_LIVRE_HPP

#include <string>
#include <fstream>
#include <vector>
#include <ostream>

using namespace std;


class Livre {
private:
    string _titre;
    string _auteur;
    int _annee;

    static bool checkContains(const string &toCheck, const string &toContains);

    static vector<string> split(string toSplit, string delimiter);

public:
    Livre();

    Livre(string titre, string auteur, int annee);

    const string &getTitre() const;

    const string &getAuteur() const;

    int getAnnee() const;

    bool operator<(const Livre &l2) const {
        if (_titre < l2.getTitre()) {
            return true;
        } else if (_auteur < l2.getAuteur()) {
            return true;
        }
        return false;
    }

    bool operator==(const Livre &l2) const {
        return _titre == l2.getTitre()
               and _auteur == l2.getAuteur()
               and _annee == l2.getAnnee();
    }

    friend ostream &operator<<(ostream &os, const Livre &livre);

};

#endif //TP6_LIVRE_HPP
