//
// Created by rolfie on 4/8/20.
//

#include "Bibliotheque.hpp"

#include <iostream>
#include <algorithm>
#include <sstream>
#include <string>
#include <stdio.h>
#include <string.h>

using namespace std;

void Bibliotheque::afficher() const {
    cout << "Affichage de la bibliotheque" << endl;
    for (const Livre &livre: *this) {
        cout << livre << endl;
    }
}

void Bibliotheque::trierParAuteurEtTitre() {
    sort(begin(), end());
}

void Bibliotheque::trierParAnnee() {
    sort(begin(), end(), [](Livre l1, Livre l2) -> bool {
        return l1.getAnnee() < l2.getAnnee();
    });
}

void Bibliotheque::ecrireFichier(const string &nomFichier) const {
    ofstream file(nomFichier);
    if (file.is_open()) {
        for (const Livre &livre : *this) {
            file << livre << endl;
        }
        file.close();
        return;
    }
    throw string("Le fichier ne s'est pas ouvert.");
}

void Bibliotheque::lireFichier(const string &nomFichier) {
    ifstream file(nomFichier);
    if (file.is_open()) {
        string delimiter = ";";
        string line;
        while (getline(file, line)) {
            vector<string> splittedString = splitString(line, delimiter);
            push_back(Livre(splittedString.at(0), splittedString.at(1), stoi(splittedString.at(2))));
        }
        file.close();
        return;
    }
    throw string("erreur : lecture du fichier impossible");
}

vector<string> Bibliotheque::splitString(string toSplit, string delimiter) const {
    vector<string> splitted;
    size_t current, previous = 0;
    current = toSplit.find(delimiter);
    while (current != string::npos) {
        splitted.push_back(toSplit.substr(previous, current - previous));
        previous = current + 1;
        current = toSplit.find(delimiter, previous);
    }
    //on ajoute le dernier
    splitted.push_back(toSplit.substr(previous, toSplit.length() - previous));
    return splitted;
}
