//
// Created by rolfie on 4/8/20.
//

#ifndef TP6_BIBLIOTHEQUE_HPP
#define TP6_BIBLIOTHEQUE_HPP

#include <vector>
#include "Livre.hpp"

using namespace std;

class Bibliotheque : public vector<Livre> {
private:
    vector<string> splitString(string toSplit, string delimiter) const;

public:
    void afficher() const;

    void trierParAuteurEtTitre();

    void trierParAnnee();

    void lireFichier(const string &nomFichier);

    void ecrireFichier(const string &nomFichier) const;
};

#endif //TP6_BIBLIOTHEQUE_HPP
