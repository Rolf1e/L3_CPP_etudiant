//
// Created by rolfie on 3/27/20.
//

#include <iostream>
#include <string>
#include "Livre.hpp"

using namespace std;

Livre::Livre() :
        _titre("Default"), _auteur("Tigran"), _annee(2020) {}

Livre::Livre(string titre, string auteur, int annee) :
        _auteur(auteur), _annee(annee) {

    if (checkContains(titre, ";")) {
        throw string("erreur : titre non valide (';' non autorisé)");
    }

    if (checkContains(titre, "\n")) {
        throw string("erreur : titre non valide ('\n' non autorisé)");
    }

    if (checkContains(auteur, ";")) {
        throw string("erreur : auteur non valide (';' non autorisé)");
    }

    if (checkContains(auteur, "\n")) {
        throw string("erreur : auteur non valide ('\n' non autorisé)");
    }

    _titre = titre;
    _auteur = auteur;
}

const string &Livre::getTitre() const {
    return _titre;
}

const string &Livre::getAuteur() const {
    return _auteur;
}

int Livre::getAnnee() const {
    return _annee;
}

bool Livre::checkContains(const string &toCheck, const string &toContains) {
    if (toCheck.find(toContains) == string::npos) {
        return false;
    };
    return true;
}

vector<string> Livre::split(string toSplit, string delimiter) {
    vector<string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = toSplit.find(delimiter, prev);
        if (pos == string::npos) pos = toSplit.length();
        string token = toSplit.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delimiter.length();
    }
    while (pos < toSplit.length() && prev < toSplit.length());
    return tokens;
}

ostream &operator<<(ostream &os, const Livre &livre) {
    os << livre._titre << ";" << livre._auteur << ";" << livre._annee;
    return os;
}
