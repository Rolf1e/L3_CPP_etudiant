//
// Created by rolfie on 4/9/20.
//
#include <CppUTest/CommandLineTestRunner.h>
#include "../main/Image.hpp"

TEST_GROUP(GroupImage) {
};

TEST(GroupImage, getPixel) {
    Image image(4, 4);
    CHECK_EQUAL(image.getPixel(0, 0), 0)
    CHECK_EQUAL(image.getPixel(2, 2), 10)
    CHECK_EQUAL(image.getPixel(2, 3), 11)
    CHECK_EQUAL(image.getPixel(3, 1), 13)
    CHECK_EQUAL(image.getPixel(0, 3), 3)
}

TEST(GroupImage, setPixel) {
    Image image(4, 4);
    image.setPixel(1, 1, 4);
    image.setPixel(2, 3, 18);
    CHECK_EQUAL(image.getPixel(1, 1), 4);
    CHECK_EQUAL(image.getPixel(2, 3), 18);
}

TEST(GroupImage, getPixelByReference) {
    Image image(4, 4);
    int &ref = image.getPixelByReference(1, 1);
    CHECK_EQUAL(ref, 5);
    ref = 18;
    CHECK_EQUAL(ref, 18);
}

TEST(GroupImage, constructorCopie) {
    Image im1(4, 4);
    Image im2(im1);
    CHECK_EQUAL(4, im2.getLargeur());
    CHECK_EQUAL(4, im2.getHauteur());
}

TEST(GroupImage, operatorEqual) {
    Image im1(4, 4);
    Image im2 = im1;
    CHECK_EQUAL(4, im2.getLargeur());
    CHECK_EQUAL(4, im2.getHauteur());
}
