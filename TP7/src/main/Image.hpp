//
// Created by rolfie on 4/9/20.
//

#ifndef TP7_IMAGE_HPP
#define TP7_IMAGE_HPP

using namespace std;

#include <string>

class Image {
private:
    int _largeur;
    int _hauteur;
    int *_pixels;
public:

    Image(int largeur, int hauteur);

    ~Image();

    Image(const Image& image);

    int getLargeur() const;

    int getHauteur() const;

    int getPixel(int i, int j) const;

    void setPixel(int i, int j, int couleur);

    int &getPixelByReference(int i, int j);

    Image& operator=(const Image& image);

};

#endif //TP7_IMAGE_HPP
