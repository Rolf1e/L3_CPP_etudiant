//
// Created by rolfie on 4/9/20.
//

#include "Image.hpp"

#include <string>

using namespace std;


Image::Image(int largeur, int hauteur) : _largeur(largeur), _hauteur(hauteur), _pixels(new int[_largeur * _hauteur]) {
    for (int i = 0; i < _largeur * _hauteur; ++i) {
        _pixels[i] = i;
    }
}

Image::Image(const Image &image) {
    _largeur = image._largeur;
    _hauteur = image._hauteur;
    _pixels = new int[_largeur * _hauteur];
    for (int i = 0; i < _largeur * _hauteur; ++i) {
        _pixels[i] = i;
    }
}

Image::~Image() {
    delete[] _pixels;
}

Image &Image::operator=(const Image &image) {
    _largeur = image._largeur;
    _hauteur = image._hauteur;
    if ((_largeur * _hauteur) != (image._hauteur * image._largeur)) {
        delete[] _pixels;
    }
    _pixels = new int[_largeur * _hauteur];
    for (int i = 0; i < _largeur * _hauteur; ++i) {
        _pixels[i] = i;
    }
    return *this;
}

int Image::getHauteur() const {
    return _hauteur;
}

int Image::getLargeur() const {
    return _largeur;
}

/**
 *
 * @param i largeur
 * @param j hauteur
 * @return
 */
int Image::getPixel(int i, int j) const {
    return _pixels[_largeur * i + j];
}

// 0 | 1 | 2 | 3
//_______________
// 4 | 5 | 6 | 7
//_______________
// 8 | 9 | 10| 11
//_______________
// 12| 13| 14| 15

void Image::setPixel(int i, int j, int couleur) {
    _pixels[_largeur * i + j] = couleur;
}

int &Image::getPixelByReference(int i, int j) {
    return _pixels[_largeur * i + j];
}

