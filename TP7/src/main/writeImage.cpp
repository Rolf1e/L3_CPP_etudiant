//
// Created by rolfie on 4/9/20.
//

#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

void ecrirePnm(const Image &img, const string &nomFichier) {
    ofstream file(nomFichier);
    if (file.is_open()) {
        file << "P2 " << img.getLargeur() << " " << img.getHauteur() << " 255\n";

        for (int i = 0; i < img.getLargeur(); ++i) {
            for (int j = 0; j < img.getHauteur(); ++j) {
                file << img.getPixel(i, j) << " ";
            }
            file << "\n";
        }
        file.close();
        return;
    }
    throw string("Failed to write in file");
}

void remplir(Image &img) {
    double invL = 6 * M_PI / double(img.getLargeur());
    for (int i = 0; i < img.getHauteur(); ++i) {
        for (int j = 0; j < img.getLargeur(); ++j) {
            int couleur = 127.f * (1.f + cos(invL * double(j)));
            img.setPixel(i, j, couleur);
        }
    }
}

Image bordure(const Image & img, int couleur, int epaisseur){
    Image image(img);
    for(int i = 0; i < image.getLargeur(); ++i){
        for (int j = 0; j < epaisseur; ++j) {
            image.setPixel(i, j, couleur);
        }
        for (int j = image.getHauteur() - epaisseur; j < image.getHauteur(); ++j) {
            image.setPixel(i, j, couleur);
        }
    }

    for(int i = 0; i < image.getHauteur(); ++i){
        for (int j = 0; j < epaisseur; ++j) {
            image.setPixel(j, i, couleur);
        }
        for (int j = image.getLargeur() - epaisseur; j < image.getLargeur(); ++j) {
            image.setPixel(j, i, couleur);
        }
    }
    return image;
}


